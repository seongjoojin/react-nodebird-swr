import { atomWithStorage } from 'jotai/utils';

export const meAtom = atomWithStorage('me', null);
