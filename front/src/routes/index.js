import React from 'react';
import loadable from '@loadable/component';
import { Route, Switch, Redirect } from 'react-router-dom';

import AppLayout from '@/components/AppLayout';
const Home = loadable(() => import('@/pages/home'));
const Signup = loadable(() => import('@/pages/signup'));
const Profile = loadable(() => import('@/pages/profile'));
const User = loadable(() => import('@/pages/user'));
const Post = loadable(() => import('@/pages/post'));
const Hashtag = loadable(() => import('@/pages/hashtag'));

// eslint-disable-next-line react/prop-types
const RouteWithLayout = ({ component: Component, layout: Layout, ...rest }) => (
	<Layout>
		<Route {...rest} render={routeProps => <Component {...routeProps} />} />
	</Layout>
);

const Routes = () => (
	<Switch>
		<RouteWithLayout exact path="/" component={Home} layout={AppLayout} />
		<RouteWithLayout path="/signup" component={Signup} layout={AppLayout} />
		<RouteWithLayout path="/profile" component={Profile} layout={AppLayout} />
		<RouteWithLayout
			exact
			path="/user/:id"
			component={User}
			layout={AppLayout}
		/>
		<RouteWithLayout
			exact
			path="/post/:id"
			component={Post}
			layout={AppLayout}
		/>
		<RouteWithLayout
			path="/hashtag/:tag"
			component={Hashtag}
			layout={AppLayout}
		/>
		<Redirect from="*" to="/" />
	</Switch>
);

export default Routes;
