import instance from '@/utils/axios';

const fetcher = url =>
	instance.get(url, { withCredentials: true }).then(result => result.data);

export default fetcher;
