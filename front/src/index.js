import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import dayjs from 'dayjs';
import { Provider } from 'jotai';

import 'antd/dist/antd.css';
import App from '@/components/App';

dayjs.locale('ko');

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<Provider>
				<App />
			</Provider>
		</BrowserRouter>
	</React.StrictMode>,
	document.getElementById('root'),
);
