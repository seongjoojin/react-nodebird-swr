import React, { useCallback, useState } from 'react';
import ProTypes from 'prop-types';
import styled, { createGlobalStyle } from 'styled-components';
import { Layout, Menu, Input, Row, Col } from 'antd';
import { Link, useLocation, useHistory } from 'react-router-dom';
import { useAtom } from 'jotai';

import { meAtom } from '@/store';
import UserProfile from './UserProfile';
import LoginForm from './LoginForm';

const { Header, Content } = Layout;

const AppLayout = ({ children }) => {
	const location = useLocation();
	const history = useHistory();
	const [searchInput, setSearchInput] = useState('');
	const [me] = useAtom(meAtom);

	const onChangeSearchInput = useCallback(
		e => {
			setSearchInput(e.target.value);
		},
		[searchInput],
	);

	const onSearch = useCallback(() => {
		history.push(`/hashtag/${searchInput}`);
	}, [searchInput]);

	return (
		<Layout className="layout">
			<GlobalStyle />
			<Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
				<Menu
					theme="dark"
					mode="horizontal"
					defaultSelectedKeys={[location.pathname]}
				>
					<Menu.Item key="/">
						<Link to="/">노드버드</Link>
					</Menu.Item>
					<Menu.Item key="/profile">
						<Link to="/profile">프로필</Link>
					</Menu.Item>
					<Menu.Item key="/search">
						<SearchInput
							enterButton
							value={searchInput}
							onChange={onChangeSearchInput}
							onSearch={onSearch}
						/>
					</Menu.Item>
				</Menu>
			</Header>
			<Content style={{ padding: '0 50px', marginTop: 64 }}>
				<div
					style={{
						minHeight: '400px',
						padding: '24px',
						backgroundColor: '#FFF',
					}}
				>
					{/* gutter 컬럼 사이의 간격 */}
					<Row gutter={12}>
						<Col xs={24} sm={24} md={8} lg={4} style={{ paddingTop: '12px' }}>
							{me ? <UserProfile /> : <LoginForm />}
						</Col>
						<Col xs={24} sm={24} md={16} lg={20} style={{ paddingTop: '12px' }}>
							{children}
						</Col>
					</Row>
				</div>
			</Content>
		</Layout>
	);
};

const GlobalStyle = createGlobalStyle`
  #root {
    display: flex;
    flex: 1;
    height: 100%;
  }
  .ant-row {
    margin-left: 0 !important;
    margin-right: 0 !important;
  }
  .ant-col:first-child {
    margin-left: 0 !important;
  }
  .ant-col:last-child {
    margin-right: 0 !important;
  }
  .ant-form-item-explain-error {
    font-size: 11px;
  }
`;

const SearchInput = styled(Input.Search)`
	vertical-align: middle;
`;

AppLayout.propTypes = {
	children: ProTypes.node.isRequired,
};

export default AppLayout;
