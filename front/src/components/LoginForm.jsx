import React, { useState, useEffect, useCallback } from 'react';
import { Button, message } from 'antd';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Form, Input } from 'formik-antd';
import * as Yup from 'yup';
import { LockOutlined, LoginOutlined, MailOutlined } from '@ant-design/icons';
import { Formik } from 'formik';
import { useAtom } from 'jotai';

import instance from '@/utils/axios';
import { meAtom } from '@/store';

const LoginFormSchema = Yup.object().shape({
	user_email: Yup.string()
		.email('올바르지 않은 이메일 형식 입니다.')
		.required('이메일은 필수 입력 항목 입니다.'),
	user_password: Yup.string().required('비밀번호는 필수 입력 항목 입니다.'),
});

const LoginForm = () => {
	const [loginLoading, setLoginLoading] = useState(false);
	const [loginError, setLoginError] = useState(null);
	const [action, setAction] = useState(null);
	const [, setMe] = useAtom(meAtom);

	const login = useCallback(async ({ email, password }) => {
		try {
			setLoginLoading(true);
			setLoginError(null);
			const response = await instance.post('/user/login', {
				email,
				password,
			});
			setMe(response.data);
		} catch (error) {
			setLoginError(error.response.data);
		} finally {
			setLoginLoading(false);
		}
	}, []);

	useEffect(() => {
		if (action) {
			if (loginError) {
				message.error(JSON.stringify(loginError, null, 4));
			}
			action.setSubmitting(false);
			setAction(null);
		}
	}, [loginError]);

	return (
		<Formik
			initialValues={{ user_email: '', user_password: '' }}
			validationSchema={LoginFormSchema}
			onSubmit={(values, { setSubmitting, resetForm }) => {
				login({ email: values.user_email, password: values.user_password });
				setAction({ setSubmitting, resetForm });
			}}
		>
			<FormWrapper>
				<Form.Item name="user_email">
					<Input
						name="user_email"
						type="email"
						placeholder="User Email"
						prefix={<MailOutlined />}
					/>
				</Form.Item>
				<Form.Item name="user_password">
					<Input.Password
						name="user_password"
						placeholder="Password"
						prefix={<LockOutlined />}
					/>
				</Form.Item>
				<Form.Item name="submit">
					<Button block type="primary" htmlType="submit" loading={loginLoading}>
						<LoginOutlined /> Log in
					</Button>
					Or <Link to="/signup">register now!</Link>
				</Form.Item>
			</FormWrapper>
		</Formik>
	);
};
const FormWrapper = styled(Form)`
	padding: 10px;
	box-sizing: border-box;
`;

export default LoginForm;
