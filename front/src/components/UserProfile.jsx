import React, { useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import { Avatar, Button, Card } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';
import { useAtom } from 'jotai';

import { meAtom } from '@/store';
import instance from '@/utils/axios';

const UserProfile = () => {
	const [me, setMe] = useAtom(meAtom);
	const [logoutLoading, setLogoutLoading] = useState(false);
	const logout = useCallback(async () => {
		try {
			setLogoutLoading(true);
			await instance.post('/user/logout');
			setMe(null);
		} catch (error) {
			console.log(error);
		} finally {
			setLogoutLoading(false);
		}
	}, []);
	const onLogout = useCallback(() => {
		logout();
	}, []);

	return me ? (
		<Card
			actions={[
				<div key="twit">
					<Link to={`/user/${me.id}`}>
						게시글
						<br />
						{me.Posts.length}
					</Link>
				</div>,
				<div key="followings">
					<Link to="/profile">
						팔로잉
						<br />
						{me.Followings.length}
					</Link>
				</div>,
				<div key="followers">
					<Link to="/profile">
						팔로워
						<br />
						{me.Followers.length}
					</Link>
				</div>,
			]}
		>
			<Card.Meta
				avatar={
					<Link to={`/user/${me.id}`}>
						<Avatar>{me.nickname[0]}</Avatar>
					</Link>
				}
				title={me.nickname}
			/>
			<Button
				style={{ marginTop: 15 }}
				onClick={onLogout}
				loading={logoutLoading}
			>
				<LogoutOutlined /> 로그아웃
			</Button>
		</Card>
	) : (
		<></>
	);
};

export default UserProfile;
